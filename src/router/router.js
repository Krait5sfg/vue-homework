import {createRouter, createWebHistory} from 'vue-router';
import HomePage from '../components/pages/Home-page';
import AboutPage from '../components/pages/About-page';
import GamePage from '../components/pages/Game-page';
import ContactPage from '../components/pages/Contact-page';
import NotFoundPage from '../components/pages/Not-found-page';

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: [
    {path: '/', name: HomePage, component: HomePage},
    {path: '/about', name: AboutPage, component: AboutPage},
    {path: '/game', name: GamePage, component: GamePage},
    {path: '/contact', name: ContactPage, component: ContactPage},
    {path: "/:pathMatch(.*)*", name: NotFoundPage, component: NotFoundPage} //404 page
  ]
});

export default router;