import {reactive} from "vue";

const store = {
  state: reactive({
    wide: false,
  }),
  setWide(newValue) {
    this.state.wide = newValue;
  },
};

export default store;